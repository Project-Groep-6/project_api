package io.swagger.api;

import io.swagger.model.BankAccount;

import java.util.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UseraccountApiControllerIntegrationTest {

    @Autowired
    private UseraccountApi api;

    @Test
    public void useraccountIbanDeleteTest() throws Exception {
        Long iban = 789L;
        String token = "token_example";
        ResponseEntity<Void> responseEntity = api.useraccountIbanDelete(iban, token);
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

    @Test
    public void useraccountIbanGetTest() throws Exception {
        Long iban = 789L;
        Long token = 789L;
        ResponseEntity<BankAccount> responseEntity = api.useraccountIbanGet(iban, token);
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

    @Test
    public void useraccountIbanPutTest() throws Exception {
        String token = "token_example";
        Long iban = 789L;
        BankAccount body = new BankAccount();
        ResponseEntity<BankAccount> responseEntity = api.useraccountIbanPut(token, iban, body);
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

}
