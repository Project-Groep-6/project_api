package io.swagger.api;

import io.swagger.model.BankAccount;

import java.util.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UseraccountsApiControllerIntegrationTest {

    @Autowired
    private UseraccountsApi api;

    @Test
    public void createAccountTest() throws Exception {
        Long token = 789L;
        BankAccount body = new BankAccount();
        ResponseEntity<BankAccount> responseEntity = api.createAccount(token, body);
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

    @Test
    public void getListOfAccountsTest() throws Exception {
        Long token = 789L;
        ResponseEntity<BankAccount> responseEntity = api.getListOfAccounts(token);
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

}
