package io.swagger.api;

import io.swagger.model.BankAccount;

import java.util.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountsApiControllerIntegrationTest {

    @Autowired
    private AccountsApi api;

    @Test
    public void addAccountTest() throws Exception {
        BankAccount body = new BankAccount();
        String iban = "iban_example";
        ResponseEntity<Void> responseEntity = api.addAccount(body, iban);
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

    @Test
    public void deleteAccountTest() throws Exception {
        String iban = "iban_example";
        ResponseEntity<Void> responseEntity = api.deleteAccount(iban);
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

    @Test
    public void getAccountsByIbanTest() throws Exception {
        String iban = "iban_example";
        ResponseEntity<BankAccount> responseEntity = api.getAccountsByIban(iban);
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

    @Test
    public void getAllAccountsTest() throws Exception {
        ResponseEntity<BankAccount> responseEntity = api.getAllAccounts();
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

}
