package io.swagger.api;

import io.swagger.model.Transaction;

import java.util.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionsApiControllerIntegrationTest {

    @Autowired
    private TransactionsApi api;

    @Test
    public void addTransactionTest() throws Exception {
        Transaction body = new Transaction();
        Integer ammount = 56;
        String ownAccount = "ownAccount_example";
        String contraAccount = "contraAccount_example";
        ResponseEntity<Void> responseEntity = api.addTransaction(body, ammount, ownAccount, contraAccount);
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

    @Test
    public void getAllTransactionsTest() throws Exception {
        ResponseEntity<List<Transaction>> responseEntity = api.getAllTransactions();
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

}
