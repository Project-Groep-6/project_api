package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * BankAccount
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-06-09T12:36:16.714Z[GMT]")
public class BankAccount   {
  @JsonProperty("iban")
  private String iban = null;

  @JsonProperty("userId")
  private String userId = null;

  /**
   * Gets or Sets accounttype
   */
  public enum AccounttypeEnum {
    PAY("pay"),
    
    SAVINGS("savings");

    private String value;

    AccounttypeEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static AccounttypeEnum fromValue(String text) {
      for (AccounttypeEnum b : AccounttypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }
  @JsonProperty("accounttype")
  private AccounttypeEnum accounttype = null;

  @JsonProperty("amountofmoney")
  private Integer amountofmoney = null;

  public BankAccount iban(String iban) {
    this.iban = iban;
    return this;
  }

  /**
   * Get iban
   * @return iban
  **/
  @ApiModelProperty(example = "INHBB0001234567NL", value = "")

  public String getIban() {
    return iban;
  }

  public void setIban(String iban) {
    this.iban = iban;
  }

  public BankAccount userId(String userId) {
    this.userId = userId;
    return this;
  }

  /**
   * Get userId
   * @return userId
  **/
  @ApiModelProperty(example = "3", value = "")

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public BankAccount accounttype(AccounttypeEnum accounttype) {
    this.accounttype = accounttype;
    return this;
  }

  /**
   * Get accounttype
   * @return accounttype
  **/
  @ApiModelProperty(example = "2", value = "")

  public AccounttypeEnum getAccounttype() {
    return accounttype;
  }

  public void setAccounttype(AccounttypeEnum accounttype) {
    this.accounttype = accounttype;
  }

  public BankAccount amountofmoney(Integer amountofmoney) {
    this.amountofmoney = amountofmoney;
    return this;
  }

  /**
   * Get amountofmoney
   * @return amountofmoney
  **/
  @ApiModelProperty(example = "4", value = "")

  public Integer getAmountofmoney() {
    return amountofmoney;
  }

  public void setAmountofmoney(Integer amountofmoney) {
    this.amountofmoney = amountofmoney;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BankAccount bankAccount = (BankAccount) o;
    return Objects.equals(this.iban, bankAccount.iban) &&
        Objects.equals(this.userId, bankAccount.userId) &&
        Objects.equals(this.accounttype, bankAccount.accounttype) &&
        Objects.equals(this.amountofmoney, bankAccount.amountofmoney);
  }

  @Override
  public int hashCode() {
    return Objects.hash(iban, userId, accounttype, amountofmoney);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BankAccount {\n");
    
    sb.append("    iban: ").append(toIndentedString(iban)).append("\n");
    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("    accounttype: ").append(toIndentedString(accounttype)).append("\n");
    sb.append("    amountofmoney: ").append(toIndentedString(amountofmoney)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
