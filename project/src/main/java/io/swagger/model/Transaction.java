package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Transaction
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-06-09T12:36:16.714Z[GMT]")
public class Transaction   {
  @JsonProperty("Timestamp")
  private Long timestamp = null;

  @JsonProperty("User_type")
  private Integer userType = null;

  @JsonProperty("Own_iban")
  private String ownIban = null;

  @JsonProperty("Contra_iban")
  private String contraIban = null;

  @JsonProperty("amount")
  private Double amount = null;

  public Transaction timestamp(Long timestamp) {
    this.timestamp = timestamp;
    return this;
  }

  /**
   * Get timestamp
   * @return timestamp
  **/
  @ApiModelProperty(value = "")

  public Long getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(Long timestamp) {
    this.timestamp = timestamp;
  }

  public Transaction userType(Integer userType) {
    this.userType = userType;
    return this;
  }

  /**
   * Get userType
   * @return userType
  **/
  @ApiModelProperty(value = "")

  public Integer getUserType() {
    return userType;
  }

  public void setUserType(Integer userType) {
    this.userType = userType;
  }

  public Transaction ownIban(String ownIban) {
    this.ownIban = ownIban;
    return this;
  }

  /**
   * Get ownIban
   * @return ownIban
  **/
  @ApiModelProperty(example = "NLxxINHO0xxxxxxxxx", value = "")

  public String getOwnIban() {
    return ownIban;
  }

  public void setOwnIban(String ownIban) {
    this.ownIban = ownIban;
  }

  public Transaction contraIban(String contraIban) {
    this.contraIban = contraIban;
    return this;
  }

  /**
   * Get contraIban
   * @return contraIban
  **/
  @ApiModelProperty(example = "NLxxINHO0xxxxxxxxx", value = "")

  public String getContraIban() {
    return contraIban;
  }

  public void setContraIban(String contraIban) {
    this.contraIban = contraIban;
  }

  public Transaction amount(Double amount) {
    this.amount = amount;
    return this;
  }

  /**
   * Get amount
   * @return amount
  **/
  @ApiModelProperty(value = "")

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Transaction transaction = (Transaction) o;
    return Objects.equals(this.timestamp, transaction.timestamp) &&
        Objects.equals(this.userType, transaction.userType) &&
        Objects.equals(this.ownIban, transaction.ownIban) &&
        Objects.equals(this.contraIban, transaction.contraIban) &&
        Objects.equals(this.amount, transaction.amount);
  }

  @Override
  public int hashCode() {
    return Objects.hash(timestamp, userType, ownIban, contraIban, amount);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Transaction {\n");
    
    sb.append("    timestamp: ").append(toIndentedString(timestamp)).append("\n");
    sb.append("    userType: ").append(toIndentedString(userType)).append("\n");
    sb.append("    ownIban: ").append(toIndentedString(ownIban)).append("\n");
    sb.append("    contraIban: ").append(toIndentedString(contraIban)).append("\n");
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
